# Añadir video en Trámites y Servicios

Solicitamos apoyo para incluir un video tutorial para la inscripción en el Registro Militar, aquí: https://www.gob.pe/519-inscribirte-en-el-registro-militar-obligatorio. Este es el link del video: https://www.youtube.com/watch?v=7rm0lPqOeGs. Gracias.